angular.module('starter.services', [])

.factory("Markers", function(){
  var Markers = [
    {
      "id": "0",
      "coords": {
        "latitude": "48.8647984",
        "longitude": "2.2781759"
      },
      "window": {
        "title": "Le Chenil de Paris"
      }
    },
    {
      "id": "1",
      "coords": {
        "latitude": "48.8606352",
        "longitude": "2.2853279"
      },
      "window" : {
        "title": "Fondation Brigitte Bardot"
      }
    }
  ];
  return Markers;
})

.factory("Dogs", function(){
  var favoriteDogs = [];

  return {
    addToFavorite: function(dog) {
	var i =favoriteDogs.indexOf(dog);
	if(i == -1) {
    		favoriteDogs.push(dog);
  	}
    },
    removeFromFavorite:function(dog){
	var i =favoriteDogs.indexOf(dog);
	if(i !== -1) {
        	favoriteDogs.splice(i, 1);
	}
    },
    getFavorites:function(){
    	return favoriteDogs;
    }
  };	
})

.factory("Book", function($ionicPopup){

 var openBooking = function($scope){
  	
	 var bookingPopup = $ionicPopup.show({
	   title: 'Let\'s meet', // String. The title of the popup.
	   templateUrl: 'templates/popup/dogs-book.html', // String (optional). The URL of an html template to place in the popup   body.
	   scope: $scope/*, // Scope (optional). A scope to link to the popup content.
	   buttons: [{ // Array[Object] (optional). Buttons to place in the popup footer.
	     text: 'Book',
	     type: 'button-assertive',
	     onTap: function(e) {
	       // e.preventDefault() will stop the popup from closing when tapped.
	       e.preventDefault();
	       return;
	     }
	   }]*/
	 });

  	 $scope.doBook = function(){
	    bookingPopup.close();
  	 }; 

}

 return {
   openBooking:function(dog){
	openBooking(dog);
   }
 };

});

