angular.module('starter.directives',[])

.directive('noScroll', function() {
    return {
        restrict: 'A',
        link: function($scope, $element, $attr) {
            $element.on('touchmove', function(e) {
                e.preventDefault();
            });
        }
    }
})

.directive('calendar',function(){
    return {
        restrict:'A',
        scope:{
        }
        ,
        link:function(scope,element,attrs,ctrl){
            $(element).ionCalendar();
        }
    }
})

.directive('datePicker',function(){
    return {
        restrict:'A',
        scope:{
        }
        ,
        link:function(scope,element,attrs,ctrl){
            $(element).ionDatePicker();
        }
    }
});
