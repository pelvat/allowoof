// Ionic Starter App

// angular.module is a global place for creating, registering and retrieving Angular modules
// 'starter' is the name of this angular module example (also set in a <body> attribute in index.html)
// the 2nd parameter is an array of 'requires'
// 'starter.services' is found in services.js
// 'starter.controllers' is found in controllers.js
angular.module('starter', ['ionic','ionic-material','uiGmapgoogle-maps','ionic.contrib.ui.tinderCards','starter.controllers', 'starter.services','starter.directives'])

.run(function($ionicPlatform) {
  $ionicPlatform.ready(function() {
    // Hide the accessory bar by default (remove this to show the accessory bar above the keyboard
    // for form inputs)
    if (window.cordova && window.cordova.plugins && window.cordova.plugins.Keyboard) {
      cordova.plugins.Keyboard.hideKeyboardAccessoryBar(true);
      cordova.plugins.Keyboard.disableScroll(true);

    }
    if (window.StatusBar) {
      // org.apache.cordova.statusbar required
      StatusBar.styleLightContent();
    }
  });
})

.config(['$stateProvider', '$urlRouterProvider','$ionicConfigProvider','uiGmapGoogleMapApiProvider',function($stateProvider, $urlRouterProvider,$ionicConfigProvider,uiGmapGoogleMapApiProvider) {

	uiGmapGoogleMapApiProvider.configure({
	    key:'AIzaSyCrMXo5p135TYqoUz0qsJikV0SqC3i7nws',
	    v: '3.17',
	    libraries: 'places,visualization'
	});
/*  uiGmapGoogleMapApiProvider.configure({
      france: true
  });*/

  $ionicConfigProvider.tabs.position("top");
  // Ionic uses AngularUI Router which uses the concept of states
  // Learn more here: https://github.com/angular-ui/ui-router
  // Set up the various states which the app can be in.
  // Each state's controller can be found in controllers.js
  $stateProvider

  // setup an abstract state for the tabs directive
  .state('dogs', {
    url: '/dogs',
    abstract: true,
    templateUrl: 'templates/tabs.html'
  })

  // Each tab has its own nav history stack:
  
  .state('dogs.selection', {
    url: '/selection',
    views: {
      'dogs': {
        templateUrl: 'templates/dogs-selection.html',
        controller: 'DogsSelectionCtrl'
      }
    }
  })
  .state('dogs.favorites', {
    url: '/favorites',
    views: {
      'favorites': {
        templateUrl: 'templates/dogs-favorites.html',
        controller: 'DogsFavoritesCtrl'
      }
    }
  })
  .state('dogs.book', {
    url: '/book',
    views: {
      'dogs': {
        templateUrl: 'templates/dogs-book.html',
        controller: 'DogsBookCtrl'
      }
    }
  })

  .state('kennels', {
    url: '/kennels',
    abstract: true,
    templateUrl: 'templates/tabs.html'
  })

  .state('kennels.scan', {
    url: '/scan',
    views: {
      'kennels': {
        templateUrl: 'templates/kennels-scan.html',
        controller: 'KennelScanCtrl'
      }
    }
  });

  // if none of the above states are matched, use this as the fallback
  $urlRouterProvider.otherwise('/dogs/selection');

}]);
