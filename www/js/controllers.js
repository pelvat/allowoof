angular.module('starter.controllers', [])


.controller('AppCtrl', function($scope,$ionicSideMenuDelegate,$timeout) {
		
	$scope.toggleHome = function(){
		$timeout(function() { 
			$ionicSideMenuDelegate./*$getByHandle('home').*/toggleRight();			
			//$ionicScrollDelegate.$getByHandle(‘kalendar’).anchorScroll(‘start’); 
		}, 10);		
	 }

})

.controller('DogsSelectionCtrl', function($scope,$ionicSideMenuDelegate,Dogs) {
  $scope.cards = [
  {
    id:1,
    name:'Pantufa',
    race:'Bichon maltais',
    picture:'img/pantufa.jpg',
    description:'So Pantufa, tenho 4 meses. Fui abandonada na rua por o dono da minha mae. Mas so um bom cao e gosto de miminhas !',
    age:"4mois",
    gender:"F"
  },
  {
    id:2,
    name:'Beethoven',
    race:'Labrador retriever',
    picture:'img/beethoven.jpg',
    description:'So Beethoven, tenho 3 meses. Eo naci ao canil e nunca vi otro lugar que a minha jaula. Gostarias de adoptarme ?',
    age:"3mois",
    gender:"M"
  },
  {
    id:3,
    name:'Blanca nieve',
    race:'Husky',
    picture:'img/blanca-nieve.jpg',
    description:'So blanca nieve, tenho 5 anos. Moreu o meu dono e fui confiada ao canil. Gostaria de encontrar um novo amigo !',
    age:"4ans",
    gender:"F"
  },  
  {
    id:4,
    name:'Pantufa',
    race:'Bichon maltais',
    picture:'img/pantufa.jpg',
    description:'So Pantufa, tenho 4 meses. Fui abandonada na rua por o dono da minha mae. Mas so um bom cao e gosto de miminhas !',
    age:"4mois",
    gender:"F"
  },
  {
    id:5,
    name:'Beethoven',
    race:'Labrador retriever',
    picture:'img/beethoven.jpg',
    description:'So Beethoven, tenho 3 meses. Eo naci ao canil e nunca vi otro lugar que a minha jaula. Gostarias de adoptarme ?',
    age:"3mois",
    gender:"M"
  },
  {
    id:6,
    name:'Blanca nieve',
    race:'Husky',
    picture:'img/blanca-nieve.jpg',
    description:'So blanca nieve, tenho 5 anos. Moreu o meu dono e fui confiada ao canil. Gostaria de encontrar um novo amigo !',
    age:"4ans",
    gender:"F"
  },  
  {
    id:7,
    name:'Pantufa',
    race:'Bichon maltais',
    picture:'img/pantufa.jpg',
    description:'So Pantufa, tenho 4 meses. Fui abandonada na rua por o dono da minha mae. Mas so um bom cao e gosto de miminhas !',
    age:"4mois",
    gender:"F"
  },
  {
    id:8,
    name:'Beethoven',
    race:'Labrador retriever',
    picture:'img/beethoven.jpg',
    description:'So Beethoven, tenho 3 meses. Eo naci ao canil e nunca vi otro lugar que a minha jaula. Gostarias de adoptarme ?',
    age:"3mois",
    gender:"M"
  },
  {
    id:9,
    name:'Blanca nieve',
    race:'Husky',
    picture:'img/blanca-nieve.jpg',
    description:'So blanca nieve, tenho 5 anos. Moreu o meu dono e fui confiada ao canil. Gostaria de encontrar um novo amigo !',
    age:"4ans",
    gender:"F"
  },  
  {
    id:10,
    name:'Pantufa',
    race:'Bichon maltais',
    picture:'img/pantufa.jpg',
    description:'So Pantufa, tenho 4 meses. Fui abandonada na rua por o dono da minha mae. Mas so um bom cao e gosto de miminhas !',
    age:"4mois",
    gender:"F"
  },
  {
    id:11,
    name:'Beethoven',
    race:'Labrador retriever',
    picture:'img/beethoven.jpg',
    description:'So Beethoven, tenho 3 meses. Eo naci ao canil e nunca vi otro lugar que a minha jaula. Gostarias de adoptarme ?',
    age:"3mois",
    gender:"M"
  },
  {
    id:12,
    name:'Blanca nieve',
    race:'Husky',
    picture:'img/blanca-nieve.jpg',
    description:'So blanca nieve, tenho 5 anos. Moreu o meu dono e fui confiada ao canil. Gostaria de encontrar um novo amigo !',
    age:"4ans",
    gender:"F"
  }];

 $scope.currentDog = $scope.cards[0];
 
    $scope.showFilters = function(){
      console.log($ionicSideMenuDelegate);
      $ionicSideMenuDelegate.$getByHandle('filters').toggleRight();
    }

    $scope.selectDog = function(index){
      $scope.currentDog = $scope.dogs[index];
    }  

    $scope.hideFilters = function(){
	console.log("hide filters");
	$ionicSideMenuDelegate.toggleRight(false);
	//$ionicSideMenuDelegate.toggleRight($scope.$$childHead);
    }

  $scope.cardDestroyed = function(index) {
    $scope.cards.splice(index, 1);
  };

  $scope.cardSwipedRight = function(dog){
    Dogs.addToFavorite(dog);
  };

   
})

.controller('FiltersCtrl', function($scope,$ionicSideMenuDelegate) {
	 $scope.hideFilters = function(){
		console.log("hide filters");
	  }
})

.controller('DogsFavoritesCtrl', function($scope,$ionicSideMenuDelegate,Dogs,ionicMaterialMotion,$timeout,Markers,Book) {
		
	$scope.$on('$ionicView.enter', function() {
	  	$scope.dogs = Dogs.getFavorites();
	  	  setTimeout(function(){
	  	    ionicMaterialMotion.fadeSlideInRight();
	  	  },300);
	  });
  

	$scope.toggleHome = function(){
		console.log($ionicSideMenuDelegate);
		$ionicSideMenuDelegate.$getByHandle('home').toggleRight();
	 };

	$scope.deleteFromFavorite = function(dog){
	  Dogs.removeFromFavorite(dog);	
	};

	$scope.book = function(dog){
	  /*$scope.map = {
	    center: { latitude: 48.858859, longitude: 2.3475569 }, 
	    zoom: 12 
	  };
	  $scope.markers = Markers;*/
	  $scope.dog=dog;
	  Book.openBooking($scope);	
	};
	
/*  var config = {
    finishDelayThrottle: 2,
    finishSpeedPercent: 0.5,
    leftOffsetPercentage: 0.8,
    selector: '#favorites-list',
    startVelocity: 1100
 };*/
 
  /* var config = {
    selector: '#favorites-list'
 };*/

})

.controller('DogsBookCtrl',function($scope){

})

.controller('KennelScanCtrl',function($scope,Markers){
  $scope.map = {
    center: { latitude: 48.858859, longitude: 2.3475569 }, 
    zoom: 12 
  };
  $scope.markers = Markers;
});
